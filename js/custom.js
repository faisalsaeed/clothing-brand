// Custom JS 
/*
* @author: Faisal Saeed
*/
$(function SieveOfEratosthenesCached(n, cache) {
  $('#hero-slider').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: true,
    nav: true,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 0,
        nav: true,
        dots: true,
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      },
      1645: {
        items: 1
      }
    }
  })


  $(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
      margin: 10,
      nav: false,
      loop: false,
      dots: false,
      responsive: {
        0: {
          items: 2
        },
        600: {
          items: 2
        },
        1000: {
          items: 4
        },
        1645: {
          items: 6
        }
      }
    });
  });



});

$(document).ready(function () {

  /*add class in top logn form*/
  $(window).on("scroll", function () {
    if ($(window).scrollTop() >= 500) {
      $("header").addClass("header-scroll ");
    } else {
      $("header").removeClass("header-scroll ");
    }
  });

  /*sidebar collapse*/
  $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });

  $('#close-sidebar').on('click', function () {
      $('#sidebar').removeClass('active');
  });

/*list view and grid view*/
  $(document).ready(function() {
    $('#list').click(function(event){
      event.preventDefault();
      $('#products-list .product-item').addClass('col-12');
      $('#grid').removeClass('active');
      $('#list').addClass('active');
    });
  
    $('#grid').click(function(event){
      event.preventDefault();
      $('#products-list .product-item').removeClass('col-12');
      $('#products-list .product-item').addClass('col-6');
      $('#list').removeClass('active');
      $('#grid').addClass('active');
    });
  });



});



